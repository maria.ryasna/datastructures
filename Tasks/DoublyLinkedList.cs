﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Tasks.DoNotChange;

namespace Tasks
{
    public class DoublyLinkedList<T> : IDoublyLinkedList<T>
    {
        public int Length => _list.Count;

        public LinkedListNode<T> First => _list.First;

        private LinkedList<T> _list { get; set; }

        public DoublyLinkedList()
        {
            _list = new LinkedList<T>();
        }

        public void Add(T e)
        { 
            if (e == null)
            {
                throw new ArgumentNullException(nameof(e));
            }
            if (_list.First == null)
            {
                _list.AddFirst(e);
            }
            else
            {
                _list.AddLast(e);
            }
        }

        public void AddAt(int index, T e)
        {
            if (e == null)
            {
                throw new ArgumentNullException(nameof(e));
            }
            if (index > Length || index < 0)
            {
                throw new IndexOutOfRangeException(nameof(index));
            }
            if (index == 0)
            {
                _list.AddFirst(e);
            }
            else if (index == Length)
            {
                _list.AddLast(e);
            }
            else
            {
                LinkedListNode<T> node = GetNodeByIndex(index);
                LinkedListNode<T> newNode = new LinkedListNode<T>(e);
                _list.AddAfter(node.Previous, newNode);
            }
        }

        public T ElementAt(int index)
        {
            if (index > Length - 1 || index < 0)
            {
                throw new IndexOutOfRangeException(nameof(index));
            }
            LinkedListNode<T> node = GetNodeByIndex(index);
            return node.Value;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new DoublyLinkedListEnumerator<T>(this);
        }

        public void Remove(T item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }
            if (_list.Contains(item))
            {
                _list.Remove(item);
            }
        }

        public T RemoveAt(int index)
        {
            if (index > Length - 1 || index < 0)
            {
                throw new IndexOutOfRangeException(nameof(index));
            }
            LinkedListNode<T> node = GetNodeByIndex(index);
            _list.Remove(node);
            return node.Value;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private LinkedListNode<T> GetNodeByIndex(int index)
        {
            LinkedListNode<T> node = _list.First;
            T value = _list.Where((_, i) => i == index).Select(value => value).FirstOrDefault();
            return _list.Find(value);
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Tasks
{
    public class DoublyLinkedListEnumerator<T> : IEnumerator<T>
    {
        private DoublyLinkedList<T> _list;
        private LinkedListNode<T> _current;

        public DoublyLinkedListEnumerator(DoublyLinkedList<T> list)
        {
            _list = list;
            _current = null;
        }

        public bool MoveNext()
        {
            if (_current == null)
            {
                _current = _list.First;
            }
            else
            {
                _current = _current.Next;
            }
            return _current != null;
        }

        public void Reset()
        {
            _current = _list.First;
        }

        public T Current
        {
            get
            {
                return _current.Value;
            }
        }

        public void Dispose() { }

        object IEnumerator.Current => Current;
    }
}

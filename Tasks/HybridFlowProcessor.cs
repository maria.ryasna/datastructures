﻿using System;
using Tasks.DoNotChange;

namespace Tasks
{
    public class HybridFlowProcessor<T> : IHybridFlowProcessor<T>
    {
        private DoublyLinkedList<T> _storage { get; set; }

        private int firstElementIndex = 0;

        public HybridFlowProcessor() 
        {
            _storage = new DoublyLinkedList<T>();
        }

        public T Dequeue()
        {
            if (_storage.Length == 0)
            {
                throw new InvalidOperationException(nameof(_storage.Length));
            }

            return _storage.RemoveAt(firstElementIndex);
        }

        public void Enqueue(T item)
        {
            _storage.Add(item);
        }

        public T Pop()
        {
            if (_storage.Length == 0)
            {
                throw new InvalidOperationException(nameof(_storage.Length));
            }

            return _storage.RemoveAt(firstElementIndex);
        }

        public void Push(T item)
        {
            _storage.AddAt(firstElementIndex, item);
        }
    }
}
